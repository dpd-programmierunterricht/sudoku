#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <curses.h>
#include <string.h>
#include <locale.h>

using namespace std; 

// sudo apt install libncurses5-dev
// g++ index.cpp -o index -lncurses

// names/025.hthttps://www.w3.org/TR/xml-entity-ml

bool hat_duplikate(const int sudoku[9]);

bool ist_gewonnen(int sudoku[9][9]) {
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9; j++) {
            if(sudoku[i][j] == 0) {
                return false;
            }
        }
    }

    return true;
}

bool ist_valide(int sudoku[9][9]) {
    // zeilen checken
    for(int zeile = 0; zeile < 9; zeile++) {
        if(hat_duplikate(sudoku[zeile])) {
            return false;
        }
    }

    // spalten checken
    for(int spalte = 0; spalte < 9; spalte++) {
        //zahlen in dieser spalte
        int array[9];
        for(int zeile = 0; zeile < 9; zeile++) {
            array[zeile] = sudoku[zeile][spalte];
        }

        // spalte checken
        if(hat_duplikate(array)) {
            return false;
        }
    }

    // kasten checken
    for(int kasten_y = 0; kasten_y < 9; kasten_y += 3) {
        for(int kasten_x = 0; kasten_x < 9; kasten_x += 3) {
            int kasten[9];
            int kastenindex = 0;

            for(int offset_y = 0; offset_y < 3; offset_y++) {
                for(int offset_x = 0; offset_x < 3; offset_x++) {
                    kasten[kastenindex] = sudoku[kasten_y + offset_y][kasten_x + offset_x];
                    kastenindex++;
                }
            }

            if(hat_duplikate(kasten)) {
                return false;
            }
        }
    }

    return true;
}

bool hat_duplikate(const int sudoku_original[9]) {
    // zeile kopieren
    int sudoku[9];
    memcpy(sudoku, sudoku_original, sizeof(int) * 9);

    // sortieren
    sort(&sudoku[0], &sudoku[9]);

    // paarweise vergleichen
    for(int i = 0; i < 8; i++) {
        if(sudoku[i] != 0 && sudoku[i] == sudoku[i + 1]) {
            return true;
        }
    }

    return false;
}

void setup_curses()
{
  // use return values.  see man pages.  likely just useful for error
  // checking (NULL or non-NULL, at least for init_scr)
  initscr();
  cbreak();
  noecho();
  // needed for cursor keys (even though says keypad)
  keypad(stdscr, true);

  start_color();
  init_pair(1, COLOR_BLACK, COLOR_WHITE);
  init_pair(0, COLOR_WHITE, COLOR_BLACK);

  curs_set(0);
}

void unset_curses()
{
  keypad(stdscr, false);
  nodelay(stdscr, false);
  nocbreak();
  echo();
  endwin();
}

void print_sudoku(int sudoku[9][9], int cursor_x, int cursor_y) {
    erase();
    move(0,0);
    addstr("╔═══════╤═══════╤═══════╗");

    for(int block = 0; block < 3; block++) {
        for(int zeile = 0; zeile < 3; zeile++) {
            move(block*4 + zeile + 1, 0);

            int i = block*3 + zeile;

            // 1. doppelstrich ausgeben
            printw("║");

            for(int y = 0; y < 9; y++) {
                if(i == cursor_x && y == cursor_y) {
                    attron(COLOR_PAIR(1));
                }

                printw(" %i", sudoku[i][y]);

                if(i == cursor_x && y == cursor_y) {
                    attroff(COLOR_PAIR(1));
                }
                
                // 2. checken ob das die 3. oder 6. zahl war und einzelstrich ausgeben
                if(y==2){
                    printw(" │");  
                }
                if(y==5){
                    printw(" │");  
                }
                
            }

            // 3. doppelstrich ausgeben
            printw(" ║");
        }

        move(block*4 + 4, 0);
        addstr("╟───────┼───────┼───────╢");
    }
    move(12, 0);
    addstr("╚═══════╧═══════╧═══════╝");
}

int main() {
    setlocale(LC_ALL,"");
    setup_curses();

    move(5, 10);
    printw("Press any key to start.");
    refresh();
    int c = getch();

    // sudodu zeichnen

    // 1. printw("text")
    // 2. move(x, y)

    // <-

    // +-------+-------+-------+
    // | 4 5 2 | 1 3 2 | 1 2 3 |
    // | 1 3 2 | 9 1 2 | 4 2 1 |
    // | 4 5 2 | 1 3 2 | 1 2 3 |
    // +-------+-------+-------+

    int original[9][9] = {
        {0,0,3, 0,2,0, 6,0,0},
        {9,0,0, 3,0,5, 0,0,1},
        {0,0,1, 8,0,6, 4,0,0},
        {0,0,8, 1,0,2, 9,0,0},
        {7,0,0, 0,0,0, 0,0,8},
        {0,0,6, 7,0,8, 2,0,0},
        {0,0,2, 6,0,9, 5,0,0},
        {8,0,0, 2,0,3, 0,0,9},
        {0,0,5, 0,1,0, 3,0,0},
    };

    int sudoku[9][9];
    memcpy(sudoku, original, 9*9*sizeof(int));

    // variable für die position vom cursor
    int cursorx = 4;
    int cursory = 4; 

    int keep_going = true;
    int zahl;
    while(keep_going) {
        print_sudoku(sudoku, cursorx, cursory);
        int c2 = getch();
        switch(c2) {
            case KEY_LEFT:
                // TODO
                if (cursory == 0) {

                } else {
                    cursory = cursory - 1;
                }
              
            break;
            case KEY_RIGHT:
                if (cursory == 8) {
                } else {
                    cursory = cursory + 1;
                }
            break;
            case KEY_DOWN:
                // TODO
                 if (cursorx == 8) {

                } else {
                 cursorx = cursorx + 1;
                }
            break;
            case KEY_UP:
                // TODO
                 if (cursorx == 0) {

                } else {
                 cursorx = cursorx - 1;
                }
            break;
            case 27:
                keep_going = false;
            break;
            case '0':
                // feld zurucksetzen
                sudoku[cursorx][cursory] = original[cursorx][cursory];
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                // character zu int
                zahl =  c2 - '0';

                if(original[cursorx][cursory] == 0) {
                    sudoku[cursorx][cursory] = zahl;

                    // zahl zurucksetzen, wenn sudoku invalide ist
                    if(!ist_valide(sudoku)) {
                       sudoku[cursorx][cursory] = 0; 
                    }

                    if(ist_gewonnen(sudoku)) {
                        keep_going = false;
                        break;
                    }
                }

                break;
            case 'z':
                memcpy(sudoku, original, 9*9*sizeof(int));
                break;
            }
    }
    
    nodelay(stdscr, true);
    erase();

    unset_curses();

    if(ist_gewonnen(sudoku)) {
        cout << "Gewonnen!" << endl;
    }

    return 0;
}

